package com.gitlab.uvarovvs.natera.test;

import com.gitlab.uvarovvs.natera.DirectedGraph;
import com.gitlab.uvarovvs.natera.Edge;
import com.gitlab.uvarovvs.natera.Graph;
import com.gitlab.uvarovvs.natera.UndirectedGraph;
import com.gitlab.uvarovvs.natera.Vertex;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GraphTest {

	private Integer first = 1;

	private String second = "B";

	private Long third = 3L;

	private Object fourth = new Object();

	private MyObject fifth = new MyObject();

	@Test
	public void testUndirectedGraph(){
		Graph graph = new UndirectedGraph();
		graph.addVertex(first);
		graph.addVertex(second);
		graph.addVertex(third);
		Edge de = graph.addEdge(fourth, fifth);
		assertEquals(graph.getVertices().size(), 5);
		graph.addVertex(fourth);
		graph.addVertex(fifth);
		assertEquals(graph.getVertices().size(), 5);
		assertThrows(IllegalArgumentException.class, () -> graph.addEdge(first, first));
		Edge ed = graph.addEdge(fifth, fourth);
		assertEquals(de, ed);
		Edge ab = graph.addEdge(first, second);
		Edge ac = graph.addEdge(first, third);
		Edge cd = graph.addEdge(third, fourth);
		List<Edge> path = graph.getPath(first, fifth);
		assertEquals(path.size(), 3);
		assertTrue(path.containsAll(Arrays.asList(ac, cd, de)));
		assertFalse(path.contains(ab));
		assertEquals(graph.getPath(first, "unreachable").size(), 0);
	}

	@Test
	public void testDirectedGraph(){
		Graph graph = new DirectedGraph();
		graph.addVertex(first);
		Vertex b = graph.addVertex(second);
		graph.addVertex(third);
		graph.addVertex(fourth);
		graph.addVertex(fifth);
		assertEquals(graph.getVertices().size(), 5);
		Edge ab = graph.addEdge(first, second);
		Edge ac = graph.addEdge(first, third);
		Edge cd = graph.addEdge(third, fourth);
		Edge de = graph.addEdge(fourth, fifth);
		List<Edge> path = graph.getPath(first, fifth);
		assertEquals(path.size(), 3);
		assertTrue(path.containsAll(Arrays.asList(ac, cd, de)));
		assertFalse(path.contains(ab));
		assertEquals(graph.getPath(fifth, first).size(), 0);
		assertEquals(ab.getDirectionality(), Edge.EdgeDirectionality.DIRECTED);
		assertEquals(b.getReachableEdges().size(), 0);
		graph.addEdge(second, first);
		assertEquals(ab.getDirectionality(), Edge.EdgeDirectionality.BIDIRECTIONAL);
		assertEquals(b.getReachableEdges().size(), 1);
		graph.addEdge(fifth, fourth);
		graph.addEdge(fourth, third);
		graph.addEdge(third, first);
		assertEquals(graph.getPath(fifth, first).size(), 3);
	}

	private static class MyObject{}
}
