package com.gitlab.uvarovvs.natera.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import com.gitlab.uvarovvs.natera.Edge;
import com.gitlab.uvarovvs.natera.Edge.EdgeDirectionality;
import com.gitlab.uvarovvs.natera.Vertex;

public class VerticesAndEdgesTest {
	
	private Integer first = 1;
	
	private String second = "B";

	private Long third = 3L;
	
	@Test
	public void testUndirectedEdges() {
		Vertex a = new Vertex(first);
		Vertex b = new Vertex(second);
		Edge edge = new Edge(a, b);
		assertEquals(a.getObject(), first);
		assertEquals(b.getObject(), second);
		assertNotEquals(a.getEdges().size(), 0);
		assertNotEquals(b.getEdges().size(), 0);
		assertTrue(a.getEdges().contains(edge));
		assertTrue(b.getEdges().contains(edge));
		assertEquals(a.getEdges().size(), a.getReachableEdges().size());
		assertEquals(a.findEdge(b), edge);
		assertNull(a.findEdge(a));
		assertEquals(edge.getSource(), a);
		assertEquals(edge.getDestination(), b);
		assertEquals(edge.getDirectionality(), EdgeDirectionality.UNDIRECTED);
		assertEquals(edge.getOppositeVertex(a), b);
		assertEquals(edge.getOppositeVertex(a).getObject(), second);
		assertEquals(edge.getOppositeVertex(b).getObject(), first);
		Vertex c = new Vertex(third);
		assertThrows(IllegalArgumentException.class, () -> c.addEdge(edge));
	}

	
	@Test
	public void testDirectedEdges() {
		Vertex a = new Vertex(first);
		Vertex b = new Vertex(second);
		Edge edge = new Edge(a, b, EdgeDirectionality.DIRECTED);
		assertEquals(a.getReachableEdges().size(), 1);
		assertEquals(b.getReachableEdges().size(), 0);
		assertEquals(edge.getDirectionality(), EdgeDirectionality.DIRECTED);
	}
	
}
