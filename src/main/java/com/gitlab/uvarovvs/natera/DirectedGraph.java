package com.gitlab.uvarovvs.natera;

import static com.gitlab.uvarovvs.natera.Edge.EdgeDirectionality.BIDIRECTIONAL;
import static com.gitlab.uvarovvs.natera.Edge.EdgeDirectionality.DIRECTED;
import static com.gitlab.uvarovvs.natera.Edge.EdgeDirectionality.UNDIRECTED;



public class DirectedGraph extends UndirectedGraph {
	
	public Edge addEdge(Object source, Object destination) {
		Edge edge = super.addEdge(source, destination);
		if (edge.getDirectionality() == UNDIRECTED) {
			edge.setDirectionality(DIRECTED); // in case of a new edge
		} else if (edge.getDirectionality() == DIRECTED && edge.getSource().getObject().equals(destination) && edge.getDestination().getObject().equals(source)) {
			edge.setDirectionality(BIDIRECTIONAL); // in case of opposite direction
		}
		return edge;
	}
	

}
