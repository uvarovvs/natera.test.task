package com.gitlab.uvarovvs.natera;

import java.util.Collection;
import java.util.List;

public interface Graph {
	
	/**
	 * adds vertex to the graph
	 */
	Vertex addVertex(Object vertex);
	
	/**
	 * adds edge to the graph
	 */
	public Edge addEdge(Object source, Object destination);
	
	/**
	 * @return a list of edges between 2 vertices. Empty list when destnation is unreachable.
	 */
	public List<Edge> getPath(Object source, Object destination);

	public Collection<Vertex> getVertices();
}
