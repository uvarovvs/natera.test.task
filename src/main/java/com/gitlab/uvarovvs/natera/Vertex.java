package com.gitlab.uvarovvs.natera;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.gitlab.uvarovvs.natera.Edge.EdgeDirectionality;

public class Vertex {
	
	private Object object;
	
	private Map<Vertex, Edge> edges = new HashMap();
	
	public Vertex(Object object) {
		this.object = object;
	}
	
	public Object getObject() {
		return object;
	}
	
	public Collection<Edge> getEdges() {
		return edges.values();
	}
	
	public Collection<Edge> getReachableEdges() {
		return edges.values().stream().filter(e -> isEdgeReachable(e)).collect(Collectors.toList());
	}
	
	public Collection<Vertex> getAdjacentVirtices() {
		return edges.keySet();
	}
	
	public Edge findEdge(Vertex oppositeVertex) {
		return edges.get(oppositeVertex);
	}
	
	public void addEdge(Edge edge) {
		Vertex opposite = edge.getOppositeVertex(this);
		if (opposite == null) {
			throw new IllegalArgumentException("The vertex is not present on the edge");
		}
		edges.put(opposite, edge);
	}
	
	private boolean isEdgeReachable(Edge edge) {
		if (edge.getDirectionality() == EdgeDirectionality.DIRECTED) {
			return edge.getSource().equals(this);
		}
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((object == null) ? 0 : object.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Vertex other = (Vertex) obj;
		if (object == null) {
			if (other.object != null) return false;
		} else if (!object.equals(other.object)) return false;
		return true;
	}
	
}
