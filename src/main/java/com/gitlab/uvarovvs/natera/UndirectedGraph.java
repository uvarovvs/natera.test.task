package com.gitlab.uvarovvs.natera;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

public class UndirectedGraph implements Graph {
	
	protected Map<Object, Vertex> vertices = new HashMap<>();
	
	public Vertex addVertex(Object vertex) {
		Vertex wrapped = vertices.get(vertex); // looking for a duplicate
		if (wrapped != null) {
			return wrapped;
		}
		wrapped = new Vertex(vertex);
		vertices.put(vertex, wrapped);
		return wrapped;
	}
	
	public Edge addEdge(Object source, Object destination) {
		if(source.equals(destination)){
			throw new IllegalArgumentException("Source and destination have to be different objects");
		}
		Vertex innerSource = addVertex(source);
		Vertex innerDestination = addVertex(destination);
		Edge edge = innerSource.findEdge(innerDestination);
		if (edge != null) {
			return edge;
		}
		return new Edge(innerSource, innerDestination);
	}
	
	public List<Edge> getPath(Object source, Object destination) {
		Map<Vertex, Edge> paths = new HashMap<>();
		Vertex innerSource = vertices.get(source);
		Vertex innerDestination = vertices.get(destination);
		vertices.values().stream().filter(v -> !v.equals(innerSource)).forEach(v -> paths.put(v, null));
		Queue<Vertex> queue = new LinkedList();
		queue.add(innerSource);
		while (!queue.isEmpty()) {
			Vertex vertex = queue.poll();
			for (Edge edge : vertex.getReachableEdges()) {
				Vertex opposite = edge.getOppositeVertex(vertex);
				if (opposite.equals(innerSource) || paths.get(opposite) != null) {
					continue;
				}
				paths.put(opposite, edge);
				queue.add(opposite);
				if (opposite.equals(innerDestination)) {
					break;
				}
			}
		}
		if (paths.get(innerDestination) == null) {
			return Collections.emptyList();
		} else {
			return getListOfEdges(innerSource, innerDestination, paths);
		}
	}
	
	protected List<Edge> getListOfEdges(Vertex s, Vertex d, Map<Vertex, Edge> paths) {
		List<Edge> edges = new ArrayList();
		for (Vertex current = d;;) {
			Edge edge = paths.get(current);
			edges.add(edge);
			current = edge.getOppositeVertex(current);
			if (current.equals(s)) {
				break;
			}
		}
		return edges;
	}

	@Override
	public Collection<Vertex> getVertices() {
		return vertices.values();
	}
}
