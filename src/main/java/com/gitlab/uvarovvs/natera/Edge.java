package com.gitlab.uvarovvs.natera;

public class Edge {
	
	public enum EdgeDirectionality {
		UNDIRECTED, DIRECTED, BIDIRECTIONAL
	}
	
	private Vertex source;
	
	private Vertex destination;
	
	private EdgeDirectionality directionality = EdgeDirectionality.UNDIRECTED;
	
	public Edge(Vertex source, Vertex destination) {
		this.source = source;
		this.destination = destination;
		source.addEdge(this);
		destination.addEdge(this);
	}
	
	public Edge(Vertex source, Vertex destination, EdgeDirectionality directionality) {
		this(source, destination);
		this.directionality = directionality;
	}
	
	public Vertex getSource() {
		return source;
	}
	
	public Vertex getDestination() {
		return destination;
	}
	
	public EdgeDirectionality getDirectionality() {
		return directionality;
	}
	
	public void setDirectionality(EdgeDirectionality directionality) {
		this.directionality = directionality;
	}
	
	public Vertex getOppositeVertex(Vertex current) {
		return source.equals(current) ? destination : destination.equals(current) ? source : null;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((destination == null) ? 0 : destination.hashCode());
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Edge other = (Edge) obj;
		if (destination == null) {
			if (other.destination != null) return false;
		} else if (!destination.equals(other.destination)) return false;
		if (source == null) {
			if (other.source != null) return false;
		} else if (!source.equals(other.source)) return false;
		return true;
	}
	
}
